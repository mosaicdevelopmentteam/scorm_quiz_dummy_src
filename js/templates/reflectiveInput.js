var ableToEnterResponse = false;
var studentEntry;
var tl = new TimelineMax();

$(document).ready(function () {

	initPage();

	if (typeof slideHasBeenCompleted !== 'undefined') {
		if (slideHasBeenCompleted == true) {
			console.log('page has been completed');
			slideEnded = false;
			$('#btnSubmit').hide();
			$('textarea').prop('disabled', true);
			$('textarea').val(getResponse());
			$('section#right').css('display', 'block');
			$('#comparison-text').css('display', 'block');
			
		} else {

		}
	}

	$("textarea").on("keyup", function () {
		if (ableToEnterResponse) {
			if ($(this).val().length) {
				$('#btnSubmit').show();
			} else {
				$('#btnSubmit').hide();
			}
		}
	});

	$('#btnSubmit').on('click', function () {
		$this = $(this);
		studentEntry = $('textarea').val();
		$this.hide();
		$('textarea').prop('disabled', true);
		$('section#right').fadeIn(function () {
			$('#comparison-text').fadeIn();
		});
		setTimeout(function () {
			$('#btnNext').removeClass('disabled')
		}, 4000)
	});
});



initPage = function () {
	ableToEnterResponse = true;
	if ($('textarea').val().length) {
		$('#btnSubmit').show();
	} else {
		$('#btnSubmit').hide();
	}
};