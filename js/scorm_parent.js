
SCORMinit();
window.addEventListener('unload', quit);
window.addEventListener('beforeunload', quit);  


// call once to initialize all scorm variables. MUST be called before any SCORM
// functions will work.
function SCORMinit() {
  ml_scorm.initSCO();
  console.log('initing scorm');
}

// Call to handle all SCORM cleanup. Must be called to preserve any student data.
function quit() {
  ml_scorm.closeSCO();
}


function makeBookmark(s) {
  let bookmark = s + '|' + curPageNum + '|' + maxPageNumReached + '|' + studentResponseString + '|' + q1 + '|' + q2 + '|' + q3 + '|' + q4 + '|' + q5 + '|' + questionTotal;
  ml_scorm.setBookmark(bookmark);
} 

exitCourse = function () {
  // EV SCORM ADDITIONS
  // mark SCO complete
  ml_scorm.completeSCO();
  ml_scorm.closeSCO();
  window.top.close();
};