studentResponseString = '';
var testing;
var data = [];
var currentObjective = 0;
var width = 1300;
var height = 740;
var scale;
var curPageNum = 0;
var maxPageNumReached = 0;//dr
var numPages;
var pagesLinear = [];
var objectiveLastSlideIndexes = [];
var objectiveLastSlideNum = 0;
var frames;
var tl;
var slideEnded = false;
var typedResponse;
var q1 = 0;
var q2 = 0;
var q3 = 0;
var q4 = 0;
var q5 = 0;
var questionTotal = 0;

// AUTOPLAY =====================
var userHasInteracted = false;
var initialPage;
// =====================

slideHasBeenCompleted = false;

$(document).ready(function () {
	frames = window.frames;
	$.getJSON("data/main.json", function (d) {
		data = d;
		init();
	}).done(function () {
		//console.log('complete');
	}).fail(function (jqXHR, textStatus, errorThrown) {
		//console.log('fail');
	}).always(function () {
		//console.log('complete whether success or fail');
	});

	function getURLParams() {
		var parameters = location.search.substring(1).split("&");
		var temp = parameters[0].split("=");
		if (getParameterByName('testing') != '' && getParameterByName('testing') != undefined) {
			testing = unescape(temp[1]);
		} else {
			testing = false;
		}

		if (testing) {
			document.onkeypress = function (e) {
				var charCodeValue = e.charCode;
				var keyTyped = String.fromCharCode(e.charCode);
				if (keyTyped == "z") {
					// AUTOPLAY =====================
					if ($('#launch-modal').length) {
						userHasInteracted = true;
						loadSlide(initialPage);
						$('#launch-modal').remove();
					} else {
						// setResponse("1");
						$('#btnNext').trigger('click');
					}
				}
			}
		}
	}

	function init() {
		getURLParams();		
		if (testing) {
			$('#scale_container').append('<div id="testadvance"></div>');
			$('#scale_container').append('<div id="status"></div>');
			//mimic an action that enables the NEXT button
			//after knowledge check, end of animation, etc.
			$('#testadvance').on('click', function () {
				$(this).hide();
				enableNext();
			});
		}


		//create linear array of page urls
		for (var i = 0; i < data.objectives.length; i++) {
			//the last page number of each objective that when clicking NEXT will increment objective #
			objectiveLastSlideNum += data.objectives[i].objectivePages.length;
			objectiveLastSlideIndexes.push(objectiveLastSlideNum);
			//create empty student response for each page in course
			for (var s = 0; s < data.objectives[i].objectivePages.length; s++) {
				//list page urls in a linear order
				pagesLinear.push(data.objectives[i].objectivePages[s]);
			}
		}

		// EV SCORM ADDITION
		let bookmark = ml_scorm.getBookmark();

		if (bookmark) {
			let unpackedBookmark = bookmark.split('|');
			initialPage = unpackedBookmark[0];
			curPageNum = parseInt(unpackedBookmark[1]);
			maxPageNumReached = parseInt(unpackedBookmark[2]);
			studentResponseString = unpackedBookmark[3];
			q1 = unpackedBookmark[4];
			q2 = unpackedBookmark[5];
			q3 = unpackedBookmark[6];
			q4 = unpackedBookmark[7];
			q5 = unpackedBookmark[8];
			questionTotal = unpackedBookmark[9];

			console.log('found bookmark: ' + bookmark);
			
		} else {
			console.log("no bookmark present loading default");
			initialPage = pagesLinear[0];
			console.log('initial page is ' + initialPage);
		}

		//how many pages are there
		numPages = pagesLinear.length;
		typedResponse = pagesLinear.indexOf("105.html");
		if (maxPageNumReached > 0) {
			$('div#launch').css('background-image', 'url("images/neca_continuebutton_transp.jpg")');
		} else {
			$('div#launch').css('background-image', 'url("images/neca_startbutton_transp.jpg")');
		}
	}

	$('#btnPrev').on('click', function () {
		$this = $(this);
		if (curPageNum > 0) {
			curPageNum--;
			loadSlide(pagesLinear[curPageNum]);
		}
		if (curPageNum == 0) {
			$this.hide();
		}
		$('#btnNext').show();
	});

	$('#btnNext').on('click', function () {
		$('.questionRem').css('display', 'none')
		$this = $(this);
		//when on any slide before exit slide
		if (curPageNum < numPages - 1) {
			curPageNum++;
			makeBookmark(pagesLinear[curPageNum]);
			// makeBookmark()	
			loadSlide(pagesLinear[curPageNum]);
		}
		//when on exit slide
		if (curPageNum == numPages - 1) {
			console.log('show conclusion slide with exit button');
			$('#btnNext').hide();
		}

		$('#btnPrev').show();
		$('#btnNext').addClass('disabled');
		$('#btnNext').removeClass('pulse');		
	});

	getResponse = function () {
		return studentResponseArray[curPageNum];
	};

	// setResponse = function (s) {
	// 	console.log('saving slide data: ' + s);
	// 	studentResponseArray[curPageNum] = s;
	// 	console.log(studentResponseArray);
	// 	ml_scorm.data = JSON.stringify(studentResponseArray);
	// };

	triggerNext = function () {
		$('#btnNext').trigger('click');
	};

	enableNext = function () {
		$('#btnNext').removeClass('disabled');
		$('#btnNext').addClass('pulse');
	};

	exitCourse = function () {
		// EV SCORM ADDITIONS
		// mark SCO complete
		ml_scorm.completeSCO();
		ml_scorm.closeSCO();
		window.top.close();
	};

	if (testing) {
		document.onkeypress = function (e) {
			var charCodeValue = e.charCode;
			var keyTyped = String.fromCharCode(e.charCode);
			if (keyTyped == "z") {
				setResponse("temp saved response");
				$('#btnNext').trigger('click');
			}
		}
	}

	enableWelcome = function () {
		$('#btn-menu header').css({
			'cursor': 'pointer',
			'pointer-events': 'auto'
		}).on('click', function () {
			loadSlide(pagesLinear[0]);
			currentObjective = 0;
			curPageNum = 0;
			$('#btnPrev').hide();
			$('#btnNext').show();
			enableNext();
		});
	};


	// AUTOPLAY =====================
	$('div#launch').on('click', function () {
		userHasInteracted = true;
		$('#launch-modal').remove();
		loadSlide(initialPage);
	});
});



loadSlide = function (s) {
	addTotal();
	$('#loader').load(s);	

	//enable next button if current page is before max page reached
	if (curPageNum < maxPageNumReached) {
		enableNext();
	}

	//if bookmarked, set maxPageNumReached to index of last slide reached
	if (curPageNum > maxPageNumReached) {
		maxPageNumReached = curPageNum;
	}

	//console.log('curPageNum: ' + curPageNum + " / numPages: " + numPages);
	if (curPageNum == numPages - 1) {
		$('#btnNext').removeClass('disabled').hide();
	} else {
		$('#btnNext').css('display', 'block');
	}

	if (testing) {
		var pageUrl = s;
		var res = pageUrl.slice(0, pageUrl.length);
		var newString = res.substr(0, res.length - 5);
		$('#status').html("Filename: <b>" + newString + "</b>");
	}

	if (curPageNum == 0) {
		$('#btnPrev').hide();
	} else {
		$('#btnPrev').show();
	}

	//EV SCORM ADDITONS
	makeBookmark(s);
	//
};

function getParameterByName(name, url) {
	if (!url) {
		url = window.location.href;
	}
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function addTotal(){
	questionTotal = q1 + q2 + q3 + q4 + q5;
	return questionTotal;
}